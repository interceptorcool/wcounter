# Arduino ESP8266 base power controller
# PlatformIO

Display SSD1306 - i2c mode:
    IO0     - #RESET
    IO13    - SDA
    IO12    - SCL

Input Counter - mode Open Drain:
    IO4     - FREQInput

Power control - mode pulse 250ms, Active LOW:
    IO5     - 80%
    IO14    - 10%
    IO16    - -2%

Alarm Input:
    IO2     - Active LOW

Reset Wifi Settings
    IO0     - switch to GND 5second

    