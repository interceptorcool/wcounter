/*
 * FREQInput.h
 *
 *  Created on: 15 апрел. 2019 г.
 *      Author: icool
 */

#pragma once

#include <Arduino.h>
#include <stdlib.h>

template <uint8_t C_PIN> class FREQInput
{
public:
	FREQInput (
		)
	{
		s_this = this;
		setPowerWeight(3200);
	}
	// Initialization
	void setup()
	{
			pinMode(C_PIN, INPUT_PULLUP);
			attachInterrupt(C_PIN, pin_change_isr, RISING);
	}

	void setPowerWeight(uint32_t count_per_KWh)
	{
		_previousMicros = 0;
		_duration = 0;
		_totalPulseCount = 0;

		_totalPower = 0.0;
		_inputFREQ = 0.0;
		_instantPower = 0.0;	
		_averagePower = 0.0;	

		_count_KWh = count_per_KWh;
		_K_convert_us_Wh = 3600.0 * 1.0e6 / _count_KWh;
	}

	double readFREQ() 			{ return _inputFREQ; }
	double readTotalPower()		{ return _totalPower; }
	double readInstantPower()	{ return _instantPower; }
	double readAverangePower()	{ return _averagePower; }
	double readDuration()		{ return (double)_duration; }

private:
	uint32_t	_count_KWh;			// количество импульсов на кВт*час
	double		_K_convert_us_Wh;	// коэффициент пересчета длительности импульса в мкС для вычисления мнгновенной мощности Вт*час

	volatile double	_totalPower;		// общая мощности кВч
	volatile double	_instantPower;		// мнгновенная Вч
	volatile double _averagePower;		// средняя мощность Вч

	volatile double	_inputFREQ;

	static FREQInput* s_this; 	// for the ISR
	
	volatile uint64_t _totalPulseCount;

	volatile uint64_t _previousMicros;
	volatile uint64_t _duration;

	static void pin_change_isr(void)
	{
		uint64_t currentMicros = micros64();
		s_this->_duration = currentMicros - s_this->_previousMicros;
		s_this->_previousMicros = currentMicros;
		s_this->_totalPulseCount++;
		if (s_this->_duration)
		{
			double __duration = (double)s_this->_duration;
			s_this->_totalPower = (double)s_this->_totalPulseCount / (double)s_this->_count_KWh;
			s_this->_instantPower = s_this->_K_convert_us_Wh / __duration;
			s_this->_averagePower = (s_this->_averagePower + s_this->_instantPower) / 2;
			s_this->_inputFREQ = 1.0e6 / __duration;
		}
	}
};

template <uint8_t C_PIN>
FREQInput<C_PIN> * FREQInput<C_PIN>::s_this;

