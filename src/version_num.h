/*
 * vertionnum.h
 *
 *  Created on: 25 мар. 2017 г.
 *      Author: icool
 */

// source file version_num.h

#ifndef VERSION_NUM_H

#define VERSION_NUM_H

// Для управления DC щеточными моторами
//#define PWM_MOTOR

#define VERSION_MAJOR 00
#define VERSION_MINOR 01

#endif // VERSION_NUM_H


