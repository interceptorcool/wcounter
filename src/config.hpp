// просто небольшая обертка для хранения параметров

#include <ArduinoJson.h>

class TConf
{

public:
	const char *filename;

private:
	JsonObject*			_jsonRootptr;
	DynamicJsonBuffer 	_jsonBuffer;

public:
	TFlyConf(const char *_filename = "/config.json")
	{
		filename = _filename;
        _jsonRootptr = nullptr;
	}

	void setup()
	{
		if (SPIFFS.exists(filename))
		{
			File JSONFile = SPIFFS.open(filename, "r");
			String rd_json = JSONFile.readString();
			JSONFile.close();

			JsonObject& root = _jsonBuffer.parseObject(rd_json);

			if (root.success())
			{
				_jsonRootptr = &root;
				//DEBUG_FLY("jsonRoot success\n");
				Serial.printf("jsonRoot success, buffer size %d\n", _jsonBuffer.size());
			} else {
				_jsonRootptr = nullptr;
				//DEBUG_FLY("jsonRoot error\n");
				Serial.print("jsonRoot error\n");
			}
		} else {
			//DEBUG_FLY("File %s not found\n", filename);
			Serial.printf("File %s not found\n", filename);
		}
	}

	bool ready() const { return _jsonRootptr != nullptr; }

	JsonObject& getRoot() const { return *_jsonRootptr; }

};
