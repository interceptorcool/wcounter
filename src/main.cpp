#include <Arduino.h>
#include <brzo_i2c.h>
#include <SSD1306Brzo.h>

#include <ESP8266WiFi.h>
// #include <ESP8266mDNS.h>
#include <FS.h>
#include <Hash.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>
#include <ESP8266HTTPUpdateServer.h>
#include <TimeLib.h>
#include <NtpClientLib.h>
#include <Ticker.h>
#include <ArduinoJson.h>

#include "MyFonts.h"
extern "C" {
	#include "user_interface.h"
}

#include <FreqMeasure.hpp>

#define ESP_EXTERNAL_NRESET 0
#define ESP_SDA             13
#define ESP_SCL             12
#define ESP_FREQINPUT       04

#define ESP_PCNT_80pct      05
#define ESP_PCNT_10pct      14
#define ESP_PCNT_MINUS      16
#define ESP_PCNT_LOGIC1     LOW
#define ESP_PCNT_LOGIC0     HIGH
#define PCNT_TIME_PULSE_MS  250

#define ESP_ALARM_INPUT     02

SSD1306Brzo  display(0x3c, ESP_SDA, ESP_SCL);
FREQInput<ESP_FREQINPUT> countInput;

// ==================================================


// ==================================================
ESP8266WebServer server(80);

const char *ssid = nullptr;
const char *wifi_password = nullptr;
const char * hostName = "esp-pwr";

StaticJsonDocument<512>     doc;


ESP8266HTTPUpdateServer *httpUpdater = nullptr;

boolean syncEventTriggered = false; // True if a time even has been triggered
NTPSyncEvent_t ntpEvent; // Last triggered event
// =====================================================

#include "version_num.h"
#include "build_defs.h"
const char completeVersion[] =
{
    VERSION_MAJOR_INIT,
    '.',
    VERSION_MINOR_INIT,
    '-', 'V', '-',
    BUILD_YEAR_CH0, BUILD_YEAR_CH1, BUILD_YEAR_CH2, BUILD_YEAR_CH3,
    '-',
    BUILD_MONTH_CH0, BUILD_MONTH_CH1,
    '-',
    BUILD_DAY_CH0, BUILD_DAY_CH1,
    ' ','T',' ',
    BUILD_HOUR_CH0, BUILD_HOUR_CH1,
    ':',
    BUILD_MIN_CH0, BUILD_MIN_CH1,
    ':',
    BUILD_SEC_CH0, BUILD_SEC_CH1,
    '\0'
};
// =====================================================


String showHead()
{
    String head = "";
    head.concat( F("\r\n\nHeap: ") );
    head.concat(system_get_free_heap_size());
    head.concat( F("\r\nBoot Vers: ") );
    head.concat(system_get_boot_version());
    head.concat( F("\r\nCPU: ") );
    head.concat(system_get_cpu_freq());
    head.concat( F("\r\nSDK: ") );
    head.concat(system_get_sdk_version());
    head.concat( F("\r\nChip ID: ") );
    head.concat(system_get_chip_id());
    head.concat( F("\r\nFlash ID: "));
    head.concat(spi_flash_get_id());
    head.concat( F("\r\nFlash Size: ") );
    head.concat(ESP.getFlashChipRealSize()/1024); Serial.println( F(" K") );
    head.concat( F("\r\nFree Space: ") );
    head.concat(ESP.getFreeSketchSpace());
    head.concat( F("\r\n") );

	return head;
}

void ResetExternalComponents()
{
    pinMode(ESP_EXTERNAL_NRESET, OUTPUT);
    delay(10);
    digitalWrite(ESP_EXTERNAL_NRESET, LOW);
    delay(500);
    digitalWrite(ESP_EXTERNAL_NRESET, HIGH);
}

Ticker displayTicker;
Ticker powerControlTicker;

uint8_t AlarmInput = 0;
Ticker checkAlarmTicker;

Ticker resetWifiConnectionTicker;

uint8_t SystemPowerContol_Enable;

enum class TPowerControlStat
{
    Start_80pct = 0,
    Start_10pct,
    Start_minus,
    Send_logic0,
    Send_logic1,
    End
};

struct st_power_control
{
    uint8_t pin;
    TPowerControlStat status;
};

static struct st_power_control stPowerControl_temp;

void PowerControlPort(struct st_power_control *target)
{
    static Ticker powerControlPortTicker;
    static uint8_t count_minus = 5;

    switch (target->status)
    {
    case TPowerControlStat::Start_10pct:
        target->pin = ESP_PCNT_10pct;
        goto start_logic0;
    case TPowerControlStat::Start_minus:
        target->pin = ESP_PCNT_MINUS;
        count_minus = 4;
        goto start_logic0;
    case TPowerControlStat::Start_80pct:
        target->pin = ESP_PCNT_80pct;
    case TPowerControlStat::Send_logic0:
    start_logic0:
        digitalWrite(target->pin, ESP_PCNT_LOGIC0);
        target->status = TPowerControlStat::Send_logic1;
        powerControlPortTicker.attach_ms<struct st_power_control *>(PCNT_TIME_PULSE_MS, PowerControlPort, target);
        break;
    case TPowerControlStat::Send_logic1:
        digitalWrite(target->pin, ESP_PCNT_LOGIC1);
        if (target->pin == ESP_PCNT_80pct)
        {
            target->status = TPowerControlStat::End;
        } else {
            if (target->pin == ESP_PCNT_10pct)
            {
                target->status = TPowerControlStat::Start_minus;
            } else {
                // ESP_PCNT_MINUS
                if (--count_minus) 
                {
                    target->status = TPowerControlStat::Send_logic0;
                } else {
                    target->status = TPowerControlStat::End;
                }
            }
        }
        powerControlPortTicker.attach_ms<struct st_power_control *>(PCNT_TIME_PULSE_MS, PowerControlPort, target);
        break;
    case TPowerControlStat::End:
        powerControlPortTicker.detach();
        break;
    }

}


double targetPower = 0.7;

void setup() 
{
    ResetExternalComponents();

    // set power control pin
    pinMode(ESP_PCNT_80pct, OUTPUT);
    digitalWrite(ESP_PCNT_80pct, ESP_PCNT_LOGIC0);
    pinMode(ESP_PCNT_10pct, OUTPUT);
    digitalWrite(ESP_PCNT_10pct, ESP_PCNT_LOGIC0);
    pinMode(ESP_PCNT_MINUS, OUTPUT);
    digitalWrite(ESP_PCNT_MINUS, ESP_PCNT_LOGIC0);
    pinMode(ESP_ALARM_INPUT, INPUT_PULLUP);

    Serial.begin(115200);
    delay(10);
    Serial.println();
    Serial.println();
    Serial.print(showHead());

    // ===================================================================
    SPIFFS.begin();
    if (!SPIFFS.exists("/formatComplete.txt"))
    {
        Serial.println("Please wait 30 secs for SPIFFS to be formatted");
        bool r = SPIFFS.format();

        Serial.printf("\n\nSpiffs formatted: %s\n", r ? "OK":"FAIL"); ;

        File f = SPIFFS.open("/formatComplete.txt", "w");
        if (!f) {
            Serial.println("file open failed");
        } else {
            f.println("Format Complete");
        }
    } else {
        Serial.println("SPIFFS is ready...");

        File file = SPIFFS.open("/config.json", "r");
        DeserializationError error = deserializeJson(doc, file);
        if (error)
            Serial.println(F("Failed to read file, using default configuration"));
        targetPower = doc["targerPower"] | 0.65;
        ssid = doc["ssid"] | "";
        wifi_password = doc["wifi_password"] | "";
        SystemPowerContol_Enable = doc["sys_pwr_enable"] | 1;
        file.close();
    }

    // ===================================================================
    WiFi.mode(WIFI_STA);
    WiFi.disconnect();
    // ************
    // ************

    //   WiFi.softAP(hostName);
    WiFi.hostname(hostName);
    // TODO: add hardware button for start SmartConfig
    if (ssid != nullptr && wifi_password != nullptr && strlen(ssid) != 0 && strlen(wifi_password) != 0)
    {
        Serial.print("connecting to :");
        Serial.print(ssid);
        WiFi.begin(ssid, wifi_password);
    } else {
        Serial.print("Wait smart configuration");
        WiFi.beginSmartConfig();
        while (!WiFi.smartConfigDone()) {
            delay(500);
            Serial.print(".");
        }
        Serial.println("");
        Serial.println("SmartConfig done.");        
        // WiFi.stopSmartConfig();
        Serial.println(WiFi.SSID());
        Serial.println(WiFi.psk());

        File file = SPIFFS.open("/config.json", "w");
        doc["ssid"] = WiFi.SSID();
        doc["wifi_password"] = WiFi.psk();

        // Serialize JSON to file
        if (serializeJson(doc, file) == 0) {
            Serial.println(F("Failed to write to file"));
        }
        file.close();        
    }

    while (WiFi.status() != WL_CONNECTED) {
        // not connected to the network
        delay(500);
        Serial.print(".");
    }
    Serial.println(" connected");

    Serial.println();
    Serial.print("IP address: ");
    Serial.println(WiFi.localIP());     // Printing IP address
    Serial.print("Signal Power: ");
    Serial.println(WiFi.RSSI());        // printing signal strength

    // MDNS.addService("http","tcp",80);

		server.on("/", HTTP_GET, [](){

			String message = R"(<head><meta http-equiv="refresh" content="10"></head><h2> Мощность с последнего включения : )";
            message.concat(String(
                countInput.readTotalPower()
            ));
            message.concat(" КВт*час<br>");
            message.concat("Мнгоновенная мощность: ");
            message.concat(String(
                countInput.readInstantPower()
            ));
			message.concat(" Вт*час<br>");
            message.concat("Усредненная мощность: ");
            message.concat(String(
                countInput.readAverangePower()
            ));
			message.concat(" Вт*час  ");
            if (!SystemPowerContol_Enable)
                message += "&#215";
            else 
            {
                if (countInput.readAverangePower() <= targetPower)
                    message += "&#8657";
                else
                    message += "&#8659";
            }
            message += "<br></h2>";

            message.concat(R"(<p><form action="/setParam" method="POST">Установка целевой мощности: <input type="number" step="0.001" min="0" placeholder="0,000"  name="Pust"  autocomplete="off" autofocus value=")");
            message.concat(String(targetPower));
            message.concat(R"("><input type="submit" value="задать"></form></p>)");

            message.concat(R"(<p><form action="/setSysEn" method="POST">система коррекции: 
            <lable><input type="radio" name="sysen" value="1" )");
            if (SystemPowerContol_Enable) message += "checked";
            message += ">ВКЛ</lable>";
            message += R"(<lable><input type="radio" name="sysen" value="0" )";
            if (!SystemPowerContol_Enable) message += "checked";
            message += ">ОТКЛ </lable>";
            message += R"(<input type="submit" value="задать"></form></p>)";


            message.concat("<p>Local time: ");
            message.concat(NTP.getTimeDateString());
            message.concat("<br>Up time: ");
            message.concat(NTP.getUptimeString());
			message.concat("</p>");

			message.concat( R"(<p><a href="/update">REPLACE SW IN Controller</a></p>)" );
			message.concat("<p>Current build: ");
			message.concat(completeVersion);
            message.concat("</p>");

			server.send(200,  "text/html; charset=UTF-8", message);
		});

		server.onNotFound([]() {
			String message = "Page not exist!\n\n";
			message += "URI: ";
			message += server.uri();
			message += "\nMethod: ";
            message += ( server.method() == HTTP_GET ) ? "GET" : "POST";
            message += "\nArguments: ";
            message += server.args();
            message += "\n";
 
            for ( uint8_t i = 0; i < server.args(); i++ ) 
            {
                message += " " + server.argName ( i ) + ": " + server.arg ( i ) + "\n";
            }   
 
            server.send ( 404, "text/plain", message );
		});

        server.on("/setSysEn", HTTP_POST, [](){
            SystemPowerContol_Enable = server.arg("sysen").toInt();
            server.sendHeader("Location", "/", true);
            server.send(303, "text/plain", "");

            File file = SPIFFS.open("/config.json", "w");
            doc["sys_pwr_enable"] = SystemPowerContol_Enable;

            // Serialize JSON to file
            if (serializeJson(doc, file) == 0) {
                Serial.println(F("Failed to write to file"));
            }
            file.close();
        });

        server.on("/setParam", HTTP_POST, [](){
            targetPower = server.arg("Pust").toFloat();
            server.sendHeader("Location", "/", true);
            server.send(303, "text/plain", "");

            File file = SPIFFS.open("/config.json", "w");
            doc["targerPower"] = targetPower;

            // Serialize JSON to file
            if (serializeJson(doc, file) == 0) {
                Serial.println(F("Failed to write to file"));
            }
            file.close();
        });


    httpUpdater = new ESP8266HTTPUpdateServer();
    httpUpdater->setup(&server);
    server.begin();    

    NTP.begin ("pool.ntp.org", 10);
    NTP.setInterval(3603);

    countInput.setup();

    powerControlTicker.attach(10*60, [](){
        if (AlarmInput != 0) return;
        if (!SystemPowerContol_Enable) return;
        if (countInput.readAverangePower() <= targetPower)
        {
            // Serial.print("+ ");
            // Serial.println(String(countInput.readAverangePower()));
            stPowerControl_temp.status = TPowerControlStat::Start_10pct;
            PowerControlPort(&stPowerControl_temp);
        } else {
            // Serial.print("- ");
            // Serial.println(String(countInput.readAverangePower()));
            stPowerControl_temp.status = TPowerControlStat::Start_80pct;
            PowerControlPort(&stPowerControl_temp);
        }
    });

    checkAlarmTicker.attach_ms(10, []{
        static uint8_t AlarmBefore = 0;
        AlarmInput = digitalRead(ESP_ALARM_INPUT) ? 0 : 1;
        if (AlarmInput != AlarmBefore)
        {
            AlarmBefore = AlarmInput;
            if (AlarmInput)
            {
                stPowerControl_temp.status = TPowerControlStat::Start_10pct;
                PowerControlPort(&stPowerControl_temp);            
                Serial.println(F("Alarm"));
            }
        }
    });

    resetWifiConnectionTicker.attach(1, []{
        static uint8_t waitPress = 5;
        
        pinMode(ESP_EXTERNAL_NRESET, INPUT_PULLUP);

        if (digitalRead(ESP_EXTERNAL_NRESET) == 0)
        {
            waitPress--;
        } else
        {
            waitPress = 5;
        };

        if (!waitPress) {
            Serial.println(F("Reset WiFi settings"));
            File file = SPIFFS.open("/config.json", "w");
            doc["ssid"] = "";
            doc["wifi_password"] = "";

            // Serialize JSON to file
            if (serializeJson(doc, file) == 0) {
                Serial.println(F("Failed to write to file"));
            }
            file.close();
            delay(100);
            ESP.restart();
        }
    });

    display.init();
    display.flipScreenVertically();
    display.setFont(Font5x7);

    displayTicker.attach_ms_scheduled(250, []() {
        // clear the display
        display.clear();
        display.setTextAlignment(TEXT_ALIGN_RIGHT);
        display.drawString(128, 56, NTP.getTimeStr());
        display.setTextAlignment(TEXT_ALIGN_LEFT);
        display.drawString(1, 56, WiFi.localIP().toString());
 

        display.drawString(1, 1, "частота:");
        display.drawString(55, 1, String(countInput.readFREQ()));
        display.drawString(1, 10, String(countInput.readDuration()));

        display.drawString(1, 20, "Pмнг Вт:");
        display.drawString(45, 20, String(countInput.readInstantPower()));
        display.drawString(70, 20, String(targetPower));
        
        display.drawString(1, 30, "Средняя:");
        display.drawString(55, 30, String(countInput.readAverangePower()));

        // display.drawString(1, 40, String(ESP.getFreeHeap()));
        display.display();
    });

}




void loop() 
{
    server.handleClient();

    // =======================================
    delay(0);
}